const { override,fixBabelImports, addDecoratorsLegacy} = require('customize-cra');

// ovverride
module.exports = override(
    fixBabelImports('import',{
        libraryName:'antd',
        libraryDirectory:'es',
        style:'css'
    }),
    addDecoratorsLegacy()
);