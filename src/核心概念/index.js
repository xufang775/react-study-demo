import React from 'react';
import ReactDom from 'react-dom';
import Clock from './Clock';

//
// const name = 'Josh Perez';
// const element = <h1>hello,{name}</h1>;

function formatName(user) {
    return user.firstName + ' ' + user.lastName;
}
const user = {
    firstName:'xufang',
    lastName:'xf'
};

function getGreeting(user) {
    if(user){
        return <div>hello,{formatName(user)}</div>;
    }
    return <h1>hello,xufang</h1>
}

const element = <h1>
    hello,{getGreeting(user)}!
</h1>;


ReactDom.render(
    element,
    document.getElementById('root')
);

function tick() {
    const element = (
        <div>
            <h1>hello,world!</h1>
            <h2>it is {new Date().toLocaleTimeString()}.</h2>
        </div>
    );
    ReactDom.render(element,document.getElementById('root'));
}
setInterval(tick,1000);

// 渲染自定义组件
function Welcome(props) {
    return <h1>hello,{props.name}</h1>;
}

function App() {
    return (
        <div>
            <Welcome name="xiaoming"></Welcome>
            <Welcome name="xiaoqing"></Welcome>
            <Welcome name="xiaohong"></Welcome>
        </div>
    );
}

const element1 = <App />;
ReactDom.render(
    element1,
    document.getElementById('root')
);