import React from 'react';
import logo from './logo.svg';
import ShoppingList from './入门教程/ShoppingList';
import './App.css';
// import Button from 'antd/lib/button';
// import "antd/dist/antd.css";
import { Button } from 'antd';

import style from "./开课吧/App.module.css";
import JsxTest from './开课吧/JsxTest';
import CompType from './开课吧/CompType';
import StateTest from "./开课吧/StateTest";
import CartSample from "./开课吧/CartSample";
import LifeCycle from "./开课吧/LifeCycle";
import HookTest from "./开课吧/HookTest";
import HookTest2 from "./开课吧/HookTest2";
import HookTest3 from "./开课吧/HookTest3";
import HookTest4 from "./开课吧/HookTest4";
import HookTest5 from "./开课吧/HookTest5";
import HookTest6 from "./开课吧/HookTest6";
import CommentList from "./开课吧/CommentList";
import Hoc from "./开课吧/Hoc";
import Composition from "./开课吧/Composition";
import AntdForm from "./开课吧/AntdForm";
import KFormTest from "./开课吧/KFormTest";
import KFormTest2 from "./开课吧/KFormTest2";
import ContextTest from "./开课吧/ContextTest";

import UseStateTest from "./Hook/UseStateTest";
import UseEffectTest from "./Hook/UseEffectTest";
import UseReducerTest from "./Hook/UseReducerTest";
import KIndex from "./源码/index";


import store from './store';
import { Provider } from 'react-redux';

function App() {
  return (
    <div className="App">
        {/*Hook Demo*/}
        {/*<UseStateTest />*/}
        {/*<UseEffectTest />*/}
        {/*<UseReducerTest/>*/}
        {/*自定义组件开头大写*/}
        {/*<JsxTest />*/}
        {/*组件类型*/}
        {/*<CompType />*/}
        {/*状态*/}
        {/*<StateTest/>*/}
        {/*循环*/}
        {/*<CartSample/>*/}
        {/*生命周期*/}
        {/*<LifeCycle/>*/}
        {/*<CommentList/>*/}
        {/*使用antd*/}
        {/*<Button type="primary">Button</Button>*/}
        {/**/}
        {/*<CommentList/>*/}
        {/*高阶组件*/}
        {/*<Hoc name={'hoc'}></Hoc>*/}
        {/*复合组件*/}
        {/*<Composition></Composition>*/}
        {/*上下文*/}
        {/*<ContextTest/>*/}
        {/*新特性Hook*/}
        {/*<HookTest />*/}
        {/*重构*/}
        {/*<HookTest2 />*/}
        {/*redux*/}
        <Provider store={store}>
            {/*<HookTest3 />*/}
            {/*<HookTest4 />*/}
            {/*路由*/}
            {/*<HookTest5 />*/}
            {/*对HookTest5中Detail组件使用嵌套*/}
            {/*<HookTest6/>*/}
        </Provider>
        <KIndex></KIndex>
        {/*antd的表单*/}
        {/*<AntdForm/>*/}
        {/*<KFormTest/>*/}
        {/*<KFormTest2/>*/}
    </div>
  );
}

export default App;
