import React, { Component,PureComponent } from 'react';

export default class CommentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments:[]
        };
    }

    componentDidMount(){
        setTimeout(()=>{
            console.log('serComments');
            this.setState({
                comments:[
                    { body: 'react is very good',author:'facebook'},
                    { body:'vue is very good', author:'youyuxi'}
                ]
            })
        },1000);
    }

    render() {
        return (
            <div >
                {this.state.comments.map((c,i) => (
                    <Comment key={i} data={c} />
                ))}
            </div>
        )
    }
}

const Comment = React.memo(function({data}) {
    console.log('render comment');
    return (
        <div>
            <p>{data.body}</p>
            <p>--- {data.author}</p>
        </div>
    );
})
//
// class Comment extends Component {
//
//     shouldComponentUpdate(props){
//         if(props.data.body === this.props.data.body
//         && props.data.author === this.props.data.author){
//
//         }
//     }
//
//     render() {
//         console.log('render comment');
//         const {data} = this.props;
//         return (
//             <div>
//                 <p>{data.body}</p>
//                 <p>--- {data.author}</p>
//             </div>
//         )
//     }
// }

