// 对HookTest3的一些简化
import React, { useState,useEffect,useReducer,useContext } from 'react';
import {connect} from 'react-redux';
import {loadingEnd,loadingStart,init,asyncFetch} from '../store'
import { BrowserRouter,Link,Route} from 'react-router-dom';


function FruitList({fruits,setFruit}) {
    return(
        fruits.map(f=>(
            <li key={f} onClick={()=>setFruit(f)}>
                <Link to={`/detail/${f}`}>{f}</Link>
            </li>
        ))
    )
}
const FruitAdd = connect()(function FruitAdd({dispatch}) {
    const [pname,setPname] = useState('');
    // const {dispatch} = useContext(Context);
    const onAddFruit = (e)=>{
        if(e.key === 'Enter'){
            // props.onAddFruit(pname);
            dispatch({type:'add',payload:pname});
            setPname('');
        }
    };
    return (
        <div>
            <input type="text"
                   value={pname}
                   onChange={e=>setPname(e.target.value)}
                   onKeyDown={onAddFruit} />
        </div>
    )
});
function Detail({match,history,location}) {
    console.log(match,history,location);
    return (
        <div>
             <h3>{match.params.fruit}的详情</h3>
            <p>...</p>
            <div>
                <button onClick={history.goBack}>返回</button>
            </div>
        </div>
    );
}
function HookTest ({fruits,loading,asyncFetch}) {
    // useState参数是状态初始值
    // 返回一个数组，第一个元素是状态变量，第二个元素是状态变更函数
    const [fruit,setFruit] = useState('草莓');

    // 使用useEffect 操作副作用
    // 请务必设置依赖选项，如果没有依赖请设置空数组，表示仅执行一次
    useEffect(()=>{
        console.log('get fruits');

        asyncFetch(['草莓','香蕉']);
        // loadingStart();
        // setTimeout(()=>{
        //     loadingEnd();
        //     init(['草莓','香蕉']);
        // },1000);
    },[]);

    useEffect(()=>{
        document.title = fruit
    },[fruit]);

    useEffect(()=>{
        const timer=setInterval(()=>{
            console.log('应用启动了');
        },1000)
        // 返回清除函数
        return function () {
            clearInterval(timer);
        }
    },[]);

    return (
        <BrowserRouter>
            <nav>
                <Link to="/">水果列表</Link>&nbsp;&nbsp;
                <Link to="/add">添加水果</Link>
            </nav>
            <div>
                <Route path="/add" component={FruitAdd}></Route>
                <Route exact path="/" render={()=>(
                    loading?(
                            <div>数据加载中...</div>
                        ):(
                            <FruitList fruits={fruits} setFruit={setFruit} />
                        )
                )}></Route>
                <Route path="/detail/:fruit" component={Detail}></Route>
            </div>

            {/*<p>{fruit ==='' ? '请选择喜爱的水果：':`您选择的是:${fruit}`}</p>*/}


        </BrowserRouter>
    )
}

const mapStateToProps = state =>({
    fruits:state.list,
    loading:state.loading
});
const mapDispatchToProps = {
    loadingStart,
    loadingEnd,
    init,
    asyncFetch
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HookTest)

