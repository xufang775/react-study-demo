import React, { useState,useEffect,useReducer,useContext } from 'react';
import {connect} from 'react-redux';

function FruitList({fruits,setFruit}) {
    return(
        fruits.map(f=>(
            <li key={f} onClick={()=>setFruit(f)}>{f}</li>
        ))
    )
}
const FruitAdd = connect()(function FruitAdd({dispatch}) {
    const [pname,setPname] = useState('');
    // const {dispatch} = useContext(Context);
    const onAddFruit = (e)=>{
        if(e.key === 'Enter'){
            // props.onAddFruit(pname);
            dispatch({type:'add',payload:pname});
            setPname('');
        }
    };
    return (
        <div>
            <input type="text"
                   value={pname}
                   onChange={e=>setPname(e.target.value)}
                   onKeyDown={onAddFruit} />
        </div>
    )
});

export default connect(state=>({
    fruits:state.list,
    loading:state.loading
}))(function HookTest ({fruits,loading,dispatch}) {
    // useState参数是状态初始值
    // 返回一个数组，第一个元素是状态变量，第二个元素是状态变更函数
    const [fruit,setFruit] = useState('草莓');
    //

    // const [fruits, setFruits] = useState(['草莓','香蕉']);
    // const [fruits, setFruits] = useState([]);

    // 使用useEffect 操作副作用
    // 请务必设置依赖选项，如果没有依赖请设置空数组，表示仅执行一次
    useEffect(()=>{
        console.log('get fruits');
        // dispatch({type:'init',payload:['草莓','香蕉']});
        dispatch({type:'loading_start'});
        setTimeout(()=>{
            // setFruits(['草莓','香蕉']);
            dispatch({type:'loading_end'});
            dispatch({type:'init',payload:['草莓','香蕉']})
        },1000);
    },[]);

    useEffect(()=>{
        document.title = fruit
    },[fruit]);

    useEffect(()=>{
        const timer=setInterval(()=>{
            console.log('应用启动了');
        },1000)
        // 返回清除函数
        return function () {
            clearInterval(timer);
        }
    },[]);

    return (
        <div>
            <p>{fruit ==='' ? '请选择喜爱的水果：':`您选择的是:${fruit}`}</p>
            {/*<FruitAdd onAddFruit={ pname => setFruits([...fruits,pname])} />*/}
            {/*<FruitAdd onAddFruit={ pname => dispatch({type:'add',payload:pname})} />*/}
            <FruitAdd/>
            {/*<FruitList fruits={fruits} setFruit={setFruit} />*/}
            {/*加载状态处理*/}
            {loading?(
                <div>数据加载中...</div>
            ):(
                <FruitList fruits={fruits} setFruit={setFruit} />
            )}
        </div>
    )
})

