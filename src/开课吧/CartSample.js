import React,{Component} from 'react';

function Cart(props) {
    return (
        <table>
            <tbody>
                {props.data.map(d =>(
                    <tr key={d.id} onClick={()=>props.onSelect(d.text)}>
                        <td>{d.text}</td>
                        <td>{d.count}</td>
                        <td>￥{d.price * d.count}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default class CartSample extends Component{
    constructor(props){
        super(props);
        this.state ={
            title:'React购物车',
            name:'开课吧',
            showTitle:false,
            goods:[
                {id:1,text:'百万年薪架构师',price:100},
                {id:2,text:'web全栈架构师',price:80},
                {id:3,text:'Python爬虫',price:60},
            ],
            cart:[]
        };
        setTimeout(()=>{
            this.setState({
                showTitle:true
            });
        },1000);
    }

    handleChange =(e)=>{
        this.setState({name:e.target.value});
    };
    addGood =(e)=>{
        this.setState({
            goods:[...this.state.goods,{id:this.state.goods.length+1,text:this.state.name,price:666}],
            name:''
        });

    };
    addToCart(good){
        const item = this.state.cart.find(c=>c.id === good.id);
        if(item){
            item.count += 1;
            this.setState({cart:[...this.state.cart]});
        } else {
            this.setState({cart:[...this.state.cart,{...good,count:1}]});
        }
    }
    // 子父通信
    onSelect =(name)=>{
        console.log(name);
    };

    render(){
        // 循环操作
        const goods = this.state.goods.map(good=>(
            <li key={good.id}>
                {good.text}
                <button onClick={e=>this.addToCart(good)}>加入购物车</button>
            </li>
        ));
        return (
            <div>
                {/*条件语句*/}
                {this.state.showTitle && <h1>{this.state.title}</h1>}
                {/*事件的处理说明*/}
                <div>
                    {/*react中单项数据源*/}
                    <input
                        type="text"
                        value={this.state.name}
                        onChange={e=>this.handleChange(e)}
                        // onChange={this.handleChange}
                    />
                    <button onClick={e=>this.addGood(e)}>添加商品</button>
                </div>
                {/*循环语句*/}
                <ul>{goods}</ul>
                {/*购物车*/}
                <Cart data={this.state.cart} onSelect={this.onSelect} />
            </div>
        );
    }
}