import React, {Component} from 'react';
// 高阶组件：
function kFormCreate(Compo) {
    return class extends Component{
        constructor(props){
            super(props);
            // 选项
            this.options = {};
            // 数据
            this.state = {};
        }
        // 处理输入事件
        handleChange=(e)=>{
            // 数据设置和核验
            const {name,value} = e.target;
            this.setState({[name]:value},()=>{
                // 单字段校验
                this.validateField(name);
            });
        };
        validateField = (field)=>{
          const rules = this.options[field].rules;
          // some 里面任何一项只要不通过就返回true跳出，取反表示检验失败
          const isValid = !rules.some(rule=>{
              if(rule.required){
                  if(!this.state[field]){
                      // 校验失败
                      this.setState({[field+'Message']:rule.message});
                      return true;
                  }
              }
              return false;
          });
            if(isValid){
              this.setState({[field+'Message']:''});
            }
            return isValid;
        };
        validateFields = (cb)=>{
            const rets = Object.keys(this.options).map(field=>this.validateField(field));
            const ret = rets.every(v=> v === true);
            cb(ret,this.state);
        };

        // 包装函 数：接收字段名和校验选项返回一个高阶组件
        getFieldDec =(field,option)=>{
            this.options[field] = option;  // 选项告诉我们如何校验
            return InputComp =>(
                <div>
                    {
                        React.cloneElement(InputComp,{
                            name:field,
                            value:this.state[field] || '',
                            onChange:this.handleChange  // 执行校验设置状态等
                        })
                    }
                </div>
            )
        };
        render(){
            return <Compo
                getFieldDec={this.getFieldDec}
                validateField={this.validateField}
                validateFields={this.validateFields}
            ></Compo>
        }
    }
}

class FormItem extends Component{
    render(){
        return (
            <div>

            </div>
        )
    }
}

@kFormCreate
class KFormTest extends Component{
    onSubmit=()=>{
        this.props.validateFields((isValid,values)=>{
            if(isValid){
                console.log(values);
                alert('登录啦！')
            } else {
                alert('校验失败！');
            }
        });
    };
    render(){
        const { getFieldDec } = this.props;
        return (
            <div>
                <div>
                    {getFieldDec('username',{
                        rules:[{required:true,message:'必填'}]
                    })(<input type="text"/>)}
                </div>
                <div>
                    {getFieldDec('password',{
                        rules:[{required:true,message:'必填'}]
                    })(<input type="password"/>)}
                </div>
                <button onClick={this.onSubmit}>登录</button>
            </div>
        );
    }
}

export default KFormTest