// import ReactDOM from 'react-dom';
import ReactDOM from './kreact-dom';
import React,{Component} from './kreact'
function Comp(props) {
    return <h2>{props.name}</h2>;
}

class Com2 extends Component{
    render(){
        return (
            <div>
                comp2
            </div>
        )
    }
}
const foo='bar';
// jsx就是js对象，就是vdom
const jsx = (
    <div id="demo" className={foo}>
        <span>hi</span>
        {/*<Comp name='kaikeba' />*/}
        {/*<Com2 name='类组件' />*/}
    </div>
);
console.log(jsx);

ReactDOM.render(jsx,document.querySelector("#root"));