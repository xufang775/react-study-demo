// 执行和vdom相关的操作
export function initVNode(vnode) {
    let {vtype}=vnode;
    if(!vtype){
        // 文本节点
        return document.createTextNode(vnode);
    }
    if(vtype === 1){
        // 原生标签
        return createNativeElement(vnode);
    } else if (vtype ===2){
        return createFunComp(vnode);
    } else {
        return createClassComp(vnode);
    }
}

function createNativeElement(vnode){
    // 1.vnode[type]
    const {type,props} = vnode;
    // 创建真正DOM
    const node = document.createElement(type);
    // 过滤特殊属性
    const {key,children,...rest} = props;
    Object.keys(rest).forEach(k=>{
        // 需要特殊处理的 htmlFor,className
        if(k === 'className'){
            node.setAttribute('class',rest[k]);
        } else if(k === 'htmlFor'){
            node.setAttribute('for',rest[k]);
        } else {
            node.setAttribute(k,rest[k]);
        }
    });
    // 递归
    children.forEach(c=>{
        node.appendChild(initVNode(c));
    });
    return node;
}
function createFunComp(vnode){

}
function createClassComp(vnode){

}