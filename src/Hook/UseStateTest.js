import  React,{useState} from 'react';
const ex1= function Example() {
    const [count,setCount] = useState(0);

    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={()=>setCount(count+1)}>Click me</button>
        </div>
    );
}

class Example extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            count:0
        }
    }
    render(){
        return (
            <div>
                <p>You clicked {this.state.count} times</p>
                <button onClick={()=>this.setState({count:this.state.count+1})}>Click me</button>
            </div>
        );
    }
}

// 复习 一下，React的函数组件

const Comp1 = (props)=>{
    // 你可以在这使用Hook
    return <div></div>
};

function Comp2(props) {
    return <div/>
}
// Hook在class 内部是不起作用的。但你可以使用它们来取代 class 。

export default Example;